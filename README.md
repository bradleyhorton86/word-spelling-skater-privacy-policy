# Privacy Policy
This privacy notice explains how TomAvaTech processes personal information and informs users of their rights and choices regarding their personal information. This notice applies to TomAvaTech’s website and mobile applications.


## 1. INTRODUCTION

TomAvaTech is committed to protecting the privacy of its users. This privacy notice outlines how TomAvaTech collects, uses, shares, and protects personal information that it receives from users who access or use its website and mobile applications.

## 2. INFORMATION WE COLLECT

TomAvaTech may collect personal information directly from users through its website and mobile applications. The information collected may include:

Contact information (such as name, email address, phone number)
Login credentials (such as username and password)
Application data (such as usage statistics)
Sensitive information (such as health information)
TomAvaTech also collects information automatically through cookies and similar technologies when users access or use its website and mobile applications.

## 3. WHAT LEGAL BASES DO WE RELY ON TO PROCESS YOUR INFORMATION?
TomAvaTech only processes personal information when it believes it is necessary and has a valid legal reason (i.e. legal basis) to do so under applicable law. These legal bases may include consent, legal obligation, vital interests, and legitimate business interests.

## 4. WHEN AND WITH WHOM DO WE SHARE YOUR PERSONAL INFORMATION?
TomAvaTech may share personal information in specific situations described in this section and/or with the following third parties:

Business Transfers. TomAvaTech may share or transfer personal information in connection with, or during negotiations of, any merger, sale of company assets, financing, or acquisition of all or a portion of its business to another company.
## 5. WHAT IS OUR STANCE ON THIRD-PARTY WEBSITES?
TomAvaTech is not responsible for the safety of any information that users share with third parties that are not affiliated with its Services, but which may link to or advertise on its Services.

## 6. HOW DO WE SECURE YOUR INFORMATION?
TomAvaTech takes appropriate measures to ensure the security and confidentiality of users' personal information. These measures include physical, electronic, and administrative safeguards to protect against unauthorized access to, disclosure of, or alteration of personal information.

## 7. WHAT ARE YOUR PRIVACY RIGHTS?
Users in certain regions have privacy rights under applicable data protection laws. These may include the right to access, rectify, restrict, and erase personal information, as well as the right to data portability and the right to object to processing. TomAvaTech will consider and act upon any request in accordance with applicable data protection laws. Users also have the right to withdraw their consent for processing personal information at any time.

## 8. CONTROLS FOR DO-NOT-TRACK FEATURES
TomAvaTech does not currently respond to Do-Not-Track browser signals, but will inform users of any changes if a standard for online tracking is adopted in the future.

## 9. DO CALIFORNIA RESIDENTS HAVE SPECIFIC PRIVACY RIGHTS?
California residents have specific rights regarding access to their personal information under the Shine The Light law, which permits users to request and obtain information about categories of personal information disclosed to third parties for direct marketing purposes. Additionally, California residents under the age of 18 have the right to request the removal of unwanted data that they publicly posted on the Services.

## 10. DO WE MAKE UPDATES TO THIS NOTICE?
TomAvaTech may update this privacy notice from time to time to stay compliant with relevant laws. Users are encouraged to review this privacy notice frequently to be informed of how their personal information is being protected.

## 11. HOW CAN YOU CONTACT US ABOUT THIS NOTICE?
If users have questions or comments about this privacy notice, they may contact TomAvaTech by email at Admin@TomAvaTech.com or by post at:

TomAvaTech
Singapore

## 12. HOW CAN YOU REVIEW, UPDATE, OR DELETE THE DATA WE COLLECT FROM YOU?
Users have the right to request access to, change, or delete their personal information collected by TomAvaTech. To do so, users can submit a data subject access request.

Thank you for using TomAvaTech's Services and reviewing our privacy policy.
